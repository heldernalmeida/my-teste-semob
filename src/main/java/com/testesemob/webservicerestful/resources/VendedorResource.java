package com.testesemob.webservicerestful.resources;

import java.util.List;

import com.testesemob.webservicerestful.models.Vendedor;
import com.testesemob.webservicerestful.repository.VendedorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * VendedorResource
 */
@RestController
@RequestMapping("/api")
public class VendedorResource {

	@Autowired
	VendedorRepository vendedorRepository;

	@GetMapping("/vendedores")
	public List<Vendedor> listaVendedores() {
		return vendedorRepository.findAll();
	}

	@GetMapping("/vendedores/{nome}")
	public List<Vendedor> listaVendedores(@PathVariable String nome) {
		return vendedorRepository.findByNome(nome);
	}

	@GetMapping("/vendedor/{id}")
	public ResponseEntity<Vendedor> listaVendedor(@PathVariable("id") long id) {
		Vendedor vendedor = vendedorRepository.findById(id);

		if (vendedor == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(vendedor);
	}

	@PostMapping("/vendedor")
	public Vendedor salvarVendedor(@RequestBody Vendedor vendedor) {
		return vendedorRepository.save(vendedor);
	}

	@PutMapping("/vendedor")
	public Vendedor atualizaVendedor(@RequestBody Vendedor vendedor) {
		return vendedorRepository.save(vendedor);
	}

	@DeleteMapping("/vendedor")
	public void deletaVendedor(@RequestBody Vendedor vendedor) {
		vendedorRepository.delete(vendedor);
	}

	@DeleteMapping("/vendedor/{id}")
	public ResponseEntity<Void> deletaVendedor(@PathVariable long id) {
		Vendedor vendedor = vendedorRepository.findById(id);

		if (vendedor == null) {
			return ResponseEntity.notFound().build();
		}
		vendedorRepository.delete(vendedor);

		return ResponseEntity.noContent().build();
	}
}