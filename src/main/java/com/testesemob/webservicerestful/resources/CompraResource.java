package com.testesemob.webservicerestful.resources;

import java.util.List;

import com.testesemob.webservicerestful.models.Compra;
import com.testesemob.webservicerestful.repository.CompraRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * CompraResource
 */
@RestController
@RequestMapping("/api")
public class CompraResource {

	@Autowired
	CompraRepository compraRepository;

	@GetMapping("/compras")
	public List<Compra> listaCompras() {
		return compraRepository.findAll();
	}

	@GetMapping("/compra/{id}")
	public ResponseEntity<Compra> listaCompra(@PathVariable("id") long id) {
		Compra compra = compraRepository.findById(id);

		if (compra == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(compra);
	}

	@PostMapping("/compra")
	public Compra salvarCompra(@RequestBody Compra compra) {
		return compraRepository.save(compra);
	}

	@PutMapping("/compra")
	public Compra atualizaCompra(@RequestBody Compra compra) {
		return compraRepository.save(compra);
	}

	@DeleteMapping("/compra")
	public void deletaCompra(@RequestBody Compra compra) {
		compraRepository.delete(compra);
	}
}