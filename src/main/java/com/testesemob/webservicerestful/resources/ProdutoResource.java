package com.testesemob.webservicerestful.resources;

import java.util.List;

import javax.validation.Valid;

import com.testesemob.webservicerestful.models.Produto;
import com.testesemob.webservicerestful.repository.ProdutoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ProdutoResource
 */
@RestController
@RequestMapping("/api")
public class ProdutoResource {

	@Autowired
	ProdutoRepository produtoRepository;

	@GetMapping("/produtos")
	public List<Produto> listaProdutos() {
		return produtoRepository.findAll();
	}

	@GetMapping("/produtos_vendedor/{vendedor}")
	public List<Produto> listaProdutosPorNomeVendedor(@PathVariable("vendedor") String vendedor) {
		return produtoRepository.findAllByNomeVendedor(vendedor);
	}

	@GetMapping("/produto/{id}")
	public ResponseEntity<Produto> listaProduto(@PathVariable("id") long id) {
		Produto produto = produtoRepository.findById(id);

		if (produto == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(produto);
	}

	@PostMapping("/produto")
	public Produto salvarProduto(@Valid @RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}

	@PutMapping("/produto")
	public Produto atualizaProduto(@Valid @RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}

	@DeleteMapping("/produto")
	public void deletaProduto(@RequestBody Produto produto) {
		produtoRepository.delete(produto);
	}
}