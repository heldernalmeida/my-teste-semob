package com.testesemob.webservicerestful.models;

/**
 * StatusPagamento
 */
public enum StatusPagamento {
	D,
	P
}