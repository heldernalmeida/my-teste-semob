package com.testesemob.webservicerestful.repository;

import java.util.List;

import com.testesemob.webservicerestful.models.Produto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * ProdutoRepository
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	Produto findById(long id);
	Produto findByNome(String nome);
	
	/**
	 * Usando JPql
	 * @param pNomeVendedor
	 * @return lista de produtos do vendedor informado
	 */
	@Query("SELECT p FROM Produto p INNER JOIN Vendedor v ON v.id = p.vendedor WHERE v.nome like %?1%")
	public List<Produto> findAllByNomeVendedor(String pNomeVendedor);
}