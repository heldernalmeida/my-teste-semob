------------------------------------------
Especificações das Chamadas ao WebService
------------------------------------------

host: http://localhost
Porta: 7888

http://localhost:7888

Operações:
    produto-resource:
        DELETE: /api/produto (Deleta um Produto)
        POST: /api/produto (Salva um produto)
            {
            	"nome": "Produto Teste",
            	"quantidade": 5,
            	"valor": 8500,
            	"vendedor_id": 1
            }
        PUT: /api/produto (Atualiza um produto)
        GET: /api/produto/{id} (Retorna um produto unico)
        GET: /api/produtos (Retorna uma lista de produtos)
        GET: /api/produtos_vendedor/{nomeVendedor} (Retorna uma lista de produtos com vendedor de nome fornecido)

    vendedor-resource:
        DELETE: /api/vendedor (Deleta um vendedor)
        POST: /api/vendedor (Salva um vendedor)
            {
            	"nome": "Fulano",
            	"email": 5,
            	"endereco": "Rua ...",
            }
        PUT: /api/vendedor (Atualiza um vendedor)
        GET: /api/vendedor/{id} (Retorna um vendedor unico)
        GET: /api/vendedores (Retorna uma lista de vendedores)

    cliente-resource:
        DELETE: /api/cliente (Deleta um cliente)
        POST: /api/cliente (Salva um cliente)
        PUT: /api/cliente (Atualiza um cliente)
        GET: /api/cliente/{id} (Retorna um cliente unico)
        GET: /api/clientes (Retorna uma lista de clientes)
    