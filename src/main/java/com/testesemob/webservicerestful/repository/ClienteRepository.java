package com.testesemob.webservicerestful.repository;

import com.testesemob.webservicerestful.models.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ClienteRepository
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	Cliente findById(long id);
	Cliente findByNome(String nome);
}