package com.testesemob.webservicerestful.repository;

import java.util.List;

import com.testesemob.webservicerestful.models.Vendedor;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * VendedorRepository
 */
public interface VendedorRepository extends JpaRepository<Vendedor, Long> {

	Vendedor findById(long id);
	List<Vendedor> findByNome(String nome);
	Vendedor findByEmail(String email);
	
}