package com.testesemob.webservicerestful.repository;

import java.util.List;

import com.testesemob.webservicerestful.models.Compra;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * CompraRepository
 */
public interface CompraRepository extends JpaRepository<Compra, Long> {

	Compra findById(long id);
	List<Compra> findByClienteId(long cliente_id);
}