package com.testesemob.webservicerestful;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.testesemob.webservicerestful.models.Produto;
import com.testesemob.webservicerestful.models.Vendedor;
import com.testesemob.webservicerestful.repository.ProdutoRepository;
import com.testesemob.webservicerestful.repository.VendedorRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
class ProdutoResourceTests {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private VendedorRepository vendedorRepository;

	// write test cases here

	@Test
	void contextLoads() {
	}

	@Test
	public void whenFindByNome_thenReturnProduto() {
		// given
		Produto produto = new Produto("Iphone 9");
		produtoRepository.save(produto);
	
		// when
		Produto found = produtoRepository.findByNome(produto.getNome());

		// then
		assertEquals(produto.getNome(), found.getNome());
	}

	@Test
	public void whenFindByNomeVendedor_thenReturnListProdutos() {
		// given
		Vendedor vendedor = new Vendedor("Harry");
		vendedor.setEmail("hp@email.com");
		vendedor.setEndereco("Rua dos Alfeneiros");
		vendedor = vendedorRepository.save(vendedor);

		Produto produto = new Produto("Coruja empalada");
		produto.setQuantidade(new BigDecimal(1));
		produto.setValor(new BigDecimal(30.00));
		produto.setVendedor(vendedor);

		Produto p = produtoRepository.save(produto);

		List<Produto> produtos = new ArrayList<Produto>();
		produtos.add(p);

		List<Produto> found = new ArrayList<Produto>();

		// when
		found = produtoRepository.findAllByNomeVendedor(vendedor.getNome());

		// then
		assertArrayEquals(produtos.toArray(), found.toArray());
	}

	@Test
	public void whenSaveProduto() {
		// given
		Produto produto = new Produto("Iphone 9");
		produtoRepository.save(produto);
	
		// when
		Produto found = produtoRepository.findByNome(produto.getNome());

		// then
		assertEquals(produto.getNome(), found.getNome());
	}

	@Test
	public void whenDeleteProduto_thenReturnEmpty() {
		// given
		Vendedor vendedor = new Vendedor("Harry");
		vendedor.setEmail("hp@email.com");
		vendedor.setEndereco("Rua dos Alfeneiros");
		vendedor = vendedorRepository.save(vendedor);

		Produto produto = new Produto("Coruja empalada");
		produto.setQuantidade(new BigDecimal(1));
		produto.setValor(new BigDecimal(30.00));
		produto.setVendedor(vendedor);

		produto = produtoRepository.save(produto);

		Produto found = produtoRepository.findById(produto.getId());
		assertEquals(produto, found);
		
		// when
		produtoRepository.delete(produto);

		// then
		found = produtoRepository.findById(produto.getId());
		assertNull(found);
	}

}
