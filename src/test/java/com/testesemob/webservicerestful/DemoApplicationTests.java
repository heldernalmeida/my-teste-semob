package com.testesemob.webservicerestful;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testesemob.webservicerestful.models.Produto;
import com.testesemob.webservicerestful.repository.ProdutoRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
class DemoApplicationTests {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Test
	void contextLoads() {
	}

	@Test
	public void whenFindByNome_thenReturnProduto() {
		// given
		Produto produto = new Produto("Iphone 9");
		produtoRepository.save(produto);
	
		// when
		Produto found = produtoRepository.findByNome(produto.getNome());

		// then
		assertEquals(produto.getNome(), found.getNome());
	}

}
